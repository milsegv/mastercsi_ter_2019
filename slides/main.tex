%
% Permission is granted to copy, distribute and/or modify this
% document under the terms of the Creative Common by-nc-sa License
% version 3.0 (CC BY-NC-SA 3.0). A copy of the license can be found at
% http://creativecommons.org/licenses/by-nc-sa/3.0/legalcode.
%

\usepackage[french]{babel}
\usepackage{caption}
\captionsetup{font=scriptsize,labelformat=empty}


% Highlight macros
\newcommand{\highlight}[1]{\textcolor{structure.fg}{\bfseries #1}}

%% Title, subtitle, authors, institute, date, ...
\title{Covert channel, Spectre et Meltdown}

\author[B. Faurot, E. Josso]{Bérenger Faurot\\[-.25em]
\texttt{\scriptsize <berenger.faurot@etu.u-bordeaux.fr>}\\
Emile Josso \\[-.25em]
\texttt{\scriptsize <emile.josso@etu.u-bordeaux.fr>}}

\institute[Master 1 CSI, France]{Master 1 CSI, Université de Bordeaux, France}

\date{19 avril 2019}

%%%%%%%%%%%%%%%%%%%%%%%%%%[ Document ]%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}

\begin{frame}
  \vspace{3.5em}
  \titlepage

  \begin{center}
    \includegraphics[scale=.2]{cc-by-nc-sa.pdf}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Plan}
  \tableofcontents
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}

\begin{frame}
  \frametitle{Introduction}

  Depuis plusieurs années les processeurs embarquent de nombreuses optimisations
  \begin{itemize}
    \item Mémoires caches
    \item Calculs spéculatifs
    \item Exécution dans le désordre
    \item ...
  \end{itemize}
  \pause

    Ces optimisations ont entraîné des failles de sécurités : les attaques sur les caches
    \pause

    \begin{itemize}
        \item Flush+Reload : une attaque générique
        \item Spectre et Meltdown : deux attaques concrètes
    \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Flush+Reload}

\begin{frame}<handout:0>
  \frametitle{Plan}
  \tableofcontents[currentsection]
\end{frame}

\subsection{Présentation}

\begin{frame}[fragile]
    \frametitle{Flush+Reload - Présentation}
    \vspace{-.25em}

    \begin{itemize}
        \item Sortie en 2013
        \item Attaque générale sur les caches
        \item Utilise un covert channel (canal auxiliaire)
    \end{itemize}

\end{frame}

\subsection{Fonctionnement}
\begin{frame}[fragile]
    \frametitle{Flush+Reload - Fonctionnement (1/2)}
    \vspace{-.25em}

    \begin{columns}[T]
        \begin{column}{6cm}
            \begin{itemize}
                \setlength{\itemsep}{1em}
                \item \textbf{Caches}: mémoires petites et très proches du CPU
                \item Latence dans cache : 1 à 20 ns
                \item Latence dans RAM : 50 à 100 ns
            \end{itemize}
        \end{column}
        \begin{column}{5cm}
        \includegraphics[scale=0.40]{images/cpumemory.png}
        \hspace*{15pt}\newline\hbox{\tiny Source: https://lwn.net/Articles/252125/}%
        \captionof{figure}{Figure 1 - Schéma de l'organisation mémoire sur un ordinateur moderne}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}[fragile]
    \frametitle{Flush+Reload - Fonctionnement (2/2)}
    \vspace{-.25em}

    \begin{enumerate}
        \setlength{\itemsep}{1em}
        \item \textbf{Cache Flush}: L'attaquant enlève la variable du cache (x86: \verb=clflush=)
        \item \textbf{Accès par victime}: La victime accède à la variable
        \item \textbf{Reload}: L'attaquant accède à la variable et mesure le temps d'accès
    \end{enumerate}
    \vspace{1em}
    \centering
    \hspace{-1em}
    \includegraphics[scale=0.37]{images/flush_reload.png}
    \hspace*{15pt}\newline\hbox{\tiny Source: Flush+Reload: a High Resolution, Low Noise, L3 Cache Side-Channel Attack}%

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Spectre}

\begin{frame}<handout:0>
  \frametitle{Plan}
  \tableofcontents[currentsection]
\end{frame}

\begin{frame}[fragile]
    \frametitle{Spectre (1/3)}
    \vfill
    \begin{columns}[T]
        \begin{column}{4cm}
    \vfill
                \begin{figure}
                    \includegraphics[scale=0.27]{images/logo_spectre.png}
                    \caption{Logo officiel de l'attaque Spectre}
                \end{figure}
    \vfill
            \end{column}
        \begin{column}{6cm}
            \begin{itemize}
            \setlength{\itemsep}{1em}
            \item Révélée en Janvier 2018
            \item Grande majorité des systèmes modernes affectés
            \item Plutôt une grosse famille de failles
            \item De nouvelles failles sont trouvées régulièrement (ex: Spectre-STL, Spectre-RSB)
            \item Ici: Spectre-PHT (Pattern History Table): même espace d'adressage que la victime
            \end{itemize}
        \end{column}
    \end{columns}


\end{frame}

\begin{frame}[fragile]
    \frametitle{Spectre (2/3)}

    \begin{itemize}
        \setlength{\itemsep}{1em}
        \item Processeurs modernes extrêmement optimisés
        \begin{itemize}
            \setlength{\itemsep}{0.5em}
            \item Embarquent mécanismes d'\textbf{exécution spéculative}
            \item Permettent de "prédire" le comportement d'un programme
            \item Se basent sur de l'analyse dynamique
        \end{itemize}
        \pause
        \item \textbf{Pattern History Table}
        \begin{itemize}
            \setlength{\itemsep}{0.5em}
            \item Se souvient si un branchement conditionnel est pris via historique
            \item La résolution peut prendre du temps si la variable n'est pas en cache \item[] $\implies$ le processeur exécute et annulera en cas d'erreur
            \item[] \verb=mov (%rbx), %rcx =\\
            \verb=jge %rax=
        \end{itemize}
        \pause
        \item Spectre exploite les instructions exécutées spéculativement
        \begin{itemize}
            \item Accès restent dans le cache
        \end{itemize}
    \end{itemize}

\end{frame}

\begin{frame}[fragile]
    \frametitle{Spectre (3/3)}
        \hspace{1em}
        Code vulnérable:
        \begin{verbatim}
    if (x < tab1_size) {
        y = tab2[tab1[x]];
    }
        \end{verbatim}
    \begin{enumerate}
        \setlength{\itemsep}{1em}
        \pause
        \item L'attaquant exécute ce code plusieurs fois avec un \verb=x= valide
        \item[] $\implies$ la PHT enregistrera le saut souvent effectué
        \pause
        \item L'attaquant choisit un \verb=x= tel que \verb=(tab1+x)= pointe vers l'octet ciblé (ex: pointe vers \verb=0x04=)
        \pause
        \item Un accès mémoire arbitraire à \verb=*(tab2+tab1[x])= <=> \verb=tab2[0x04]= est effectué spéculativement
        \pause
        \item L'attaquant utilise Flush+Reload sur toutes les valeurs de \verb=tab2=
        \item[] $\implies$ il trouve la valeur de l'octet mise en cache
    \end{enumerate}

\end{frame}

\section{Meltdown}
\begin{frame}<handout:0>
  \frametitle{Plan}
  \tableofcontents[currentsection]
\end{frame}
\subsection{Présentation}
\begin{frame}
  \frametitle{Meltdown}

    \begin{columns}[c]
        \begin{column}{5cm}
                \begin{figure}
                    \includegraphics[scale=0.30]{images/logo_meltdown.png}
                    \caption{Logo officiel de l'attaque Meltdown}
                \end{figure}
            \end{column}
        \begin{column}{7.5cm}
            \begin{itemize}
                \setlength\itemsep{1em}
                \item Révélée en janvier 2018
                \item Tout comme Spectre, Meltdown utilise les caches, mais cible l'exécution dans le désordre.
                \item Affecte la grande majorité des processeurs Intel.
                \item Vulnérabilité importante : l'attaquant peut lire toutes les données stockées en mémoire!
            \end{itemize}
        \end{column}
    \end{columns}

\end{frame}
\subsection{Exécution dans le désordre}
\begin{frame}[fragile]
    \frametitle{Out-Of-Order Execution}

\begin{columns}
    \begin{column}{0.48\textwidth}
        \begin{figure}
            \includegraphics[scale=0.4] {images/outoforder.png}
    \caption{Schéma simplifié de la microarchitecture\cite{meltdown}}
        \end{figure}
    \end{column}
    \begin{column}{0.48\textwidth}
        En résumé :
        \begin{itemize}
                \item Les instructions arrivent dans le coeur
                \item On les découpe en $\mu OP$
                \item On exécute les $\mu OP$ dès qu'on le peut
                \item On réordonne les résultats
        \end{itemize}
    \end{column}
\end{columns}


\end{frame}
\subsection{Attaque Meltdown}
\begin{frame}[fragile]
  \frametitle{Principe}
    Nous avons besoin de l'adresse d'un octet que l'on veut lire (le secret) et d'un tableau \verb=T= de 256 pages. \\
    \vspace{2em}
    L'attaque se déroule en deux étapes :
    \begin{itemize}
        \item Mise en cache du secret
        \item Lecture du secret
    \end{itemize}
    \vspace{2em}
    La lecture du secret se résume en une attaque du type Flush+Reload, nous allons détailler la technique employée pour mettre en cache le secret de la mémoire.\\
\end{frame}
\begin{frame}[fragile]
  \frametitle{Attaque Meltdown}
    \begin{verbatim}
    xor rax, rax
    retry:
    mov al, byte [rcx]          ; Lecture du secret
    shl rax, 0xc                ; Multiplication par 4096
    jz retry
    mov rbx, qword [rbx + rax]  ; Mise en cache de T[secret*4096]
    \end{verbatim}
    où \verb=rcx= contient l'adresse du secret, et \verb=rbx= l'adresse du tableau \verb=T=.\\
    3ème ligne : lève une interruption (opération illégale)
    \begin{itemize}
        \item Crée un thread de gestion qui envoit un signal au programme
        \item Signal reçu $\Rightarrow$ le programme annule les instructions qui suivent l'opération illégale (\textit{race condition})
        \item On doit mettre en cache le secret avant l'arrivée du signal
    \end{itemize}
    \vspace{1em}
    \pause
    Si on gagne la \textit{race condition} contre le kernel, on a mis en cache \verb=T[secret*4096]=, une attaque du type Flush+Reload nous permettra savoir à quel indice de \verb=T= cela correspond, et donc d'en déduire la valeur du secret.\\
\end{frame}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Conclusion}


\begin{frame}
  \frametitle{Conclusion}

  \vfill
    \begin{itemize}
        \item Les optimisations ont entrainé des failles de sécurité dont certaines non patchées et de nouvelles attaques apparaissent régulièrement.
        \item Manque de transparence de la part des architectes : les failles mettent plus de temps avant d'être découvertes.
        \item Question : doit-on privilégier les performances au détriment de la robustesse?
    \end{itemize}
  \vfill

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Références \& Lectures supplémentaires}

\nocite{*}
\bibliographystyle{plain}

\begin{frame}[allowframebreaks]
  \frametitle{Références \& Lectures supplémentaires}
  \bibliography{bibliography}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \vfill
  \centering
  \highlight{\Huge Questions~?}
  \vfill
  Dépôt git: \url{http://gitlab.com/milsegv/mastercsi_ter_2019}
\end{frame}
