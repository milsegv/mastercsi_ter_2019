\chapter{Failles Hardware}\label{chapter:failles}
La faiblesse du hardware, mise en évidence par RowHammer \cite{rowhammer}, est un domaine de recherche en plein essor. Le récent PortSmash \cite{portsmash} montre que l'on est loin d'avoir épuisé toutes les attaques de ce domaine. Avec la découverte de failles sur les caches (\cite{flushreload} par exemple), la porte était ouverte pour des attaques de plus grande ampleur, aboutissant à des attaques comme Spectre et Meltdown (\cite{spectrepaper} \cite{meltdown}), permettant à un attaquant d'obtenir des informations sur des données arbitraires stockées en mémoire sans que le programme n'ait de faiblesse sémantique. Ces attaques outrepassent les méthodes de vérification formelles étant donné qu'elles exploitent des comportements hardware, supposé fiables. Nous nous proposons dans cette section d'introduire les attaques hardware sur les caches, suivis par RowHammer, Spectre et enfin Meltdown.
Fin 2018, une étude \cite{systematic} a proposé de résumer les attaques sur les caches connues, et de montrer une méthode systématique pour trouver de nouvelles failles, aboutissant à 7 nouvelles attaques. Ces attaques sont résumées dans la figure \ref{fig:attack-summary}:
\begin{figure}[H]
\centering
\includegraphics[scale=0.4]{images/attack-summary.png}
\end{figure}
\begin{figure}
\caption{Vue globale des différentes variantes des attaques Spectre et Meltdown. En rouge/gras et en bleu, attaques démontrées. En vert/haché, attaques non exploitables. Source: \protect\cite{systematic}}
\label{fig:attack-summary}
\end{figure}
\newpage

\section{Attaques par covert channel}\label{sec:canal}
\subsection{Fonctionnement}
Un \textit{covert channel} (ou canal auxiliaire/caché), est une attaque faisant fuiter de l'information par un biais non prévu par le concepteur du système. Par exemple, mesurer la consommation d'énergie d'un CPU lors du calcul d'un AES peut permettre de faire fuiter des informations sur l'étape de l'algorithme.

De nombreuses variantes de covert channels existent pour de nombreuses cibles différentes, mais nous ne considérerons ici que celles portant sur les caches CPU, en particulier la méthode Flush+Reload \cite{flushreload}, détaillée ci-dessous. Cette technique a l'avantage de fonctionner sur des environnements multi-coeur, ainsi que dans des machines virtuelles. Elle n'est pas la seule technique ciblant les failles, il en existe bien d'autres comme Prime+Probe ou Evict+Time. \
Grossièrement, Flush+Reload permet de savoir si une victime a accédé ou non à une zone mémoire qu'elle partage avec l'attaquant. Elle permet donc de cibler efficacement les librairies partagées, un attaquant pouvant deviner si la victime a utilisée ou non une fonction de la librairie.
\subsection{Flush+Reload}\label{sec:flushreload}
Nous nous plaçons dans le cas où l'attaquant a un programme s'exécutant sur le même coeur CPU, il a donc accès au cache L3 du programme victime. Cette précision est primordiale pour que les temps d'accès mesurés par l'attaquant soient corrects. Dans le cas contraire, le cache L3 de la victime ne sera pas vidé, et il est peu probable que l'attaquant mesure une différence de temps d'accès lui permettant de tirer une quelconque conclusion.

Une attaque Flush+Reload se déroule en 3 étapes:
\begin{enumerate}
\item \textbf{Cache Flush}: L'attaquant vide de la hiérarchie des caches la ligne de mémoire qu'il surveille, avec l'instruction \verb=clflush=.
\item \textbf{Accès victime}: Le programme victime accède à la ligne mémoire précédemment enlevée des caches. La ligne mémoire est donc remise dans les caches.
\item \textbf{Reload}: L'attaquant accède à la ligne mémoire, en mesurant son temps d'accès.
\end{enumerate}

Si au cours de la deuxième étape, la victime a accédé à la ligne mémoire observée, le temps d'accès mémoire mesuré par l'attaquant sera bien plus faible à cause de la présence de la ligne mémoire en cache. En revanche, si la victime n'a pas accédé à la ligne mémoire observée, le temps d'accès sera plus long, et l'attaquant en déduira que la victime n'a pas accédé à la zone mémoire observée. La figure \ref{fig:flush_reload} résume les trois étapes de l'attaque:

\begin{figure}[H]
\centering
\includegraphics[scale=0.5]{images/flush_reload.png}
\end{figure}
\begin{figure}[H]
\caption{Description schématique de l'attaque Flush+Reload. Source: \cite{flushreload}}
\label{fig:flush_reload}
\end{figure}

Dans le scénario (A), il n'y a pas d'accès mémoire de la part de la victime. L'attaque n'a donc aucun effet. Le scénario (B) lui montre l'attaque dans le cas d'un accès victime: le second Reload permettra à l'attaquant de détecter cet accès.

Afin de s'assurer de bien saisir l'accès par la victime d'une ligne de cache, un attaquant peut jouer sur la période d'attente: une plus longue période permettra de saisir l'accès avec un peu plus de certitudes, mais diminuera la granularité de l'attaque.

Il est important de noter que d'autres mécanismes d'optimisation diminuant les temps d'accès mémoire entrent en jeu, comme détaillé dans la section \ref{branchement_opti}. De plus, il est possible que la ligne de cache accédée par la victime ne soit stockée que dans les caches L1 ou L2 du coeur considéré. Dans tout les cas, il est nécéssaire de répéter l'attaque un certain nombre de fois afin d'augmenter son efficacité.

Egalement, cette attaque nécéssite de connaître à l'avance l'adresse physique de la variable utilisée par le programme victime, du moins savoir quel zone virtuelle est mappée à cet emplacement physique; on peut donc cibler efficacement les variables des librairies partagées étant donné qu'elles ont la même adresse physique qu'importe le programme y accédant. Cette attaque peut donc en pratique être utilisée pour faire fuiter des informations sur des accès à des librairies comme GnuPG, comme l'ont montré  Y. Yarom et K. Falkner \cite{flushreload}. La cible de leur Proof Of Concept est l'algorithme Square-and-multiply utilisé dans RSA permettant de calculer des exposants. L'appel aux sous-routines de cet algorithme étant entièrement défini par la représentation binaire de l'exposant secret, ils arrivent à retrouver ce dernier en mesurant les temps d'accès aux fonctions de l'algorithme.

\section{Rowhammer}
Le principe de Rowhammer \cite{rowhammer} est de faire de nombreux accès à la RAM afin de changer la valeur des bits stockés. Nous allons essayer de comprendre le fonctionnement.
\subsection{Organisation de la mémoire}
Une barette DRAM est grossièrement un groupement de puces de 1 à 8 Gigabits en \textit{ranks}, \textit{ranks} eux-mêmes groupés en \textit{modules}.
\begin{figure}[H]
\centering
\includegraphics[scale=0.4]{images/DRAMbank-cells.png}
\end{figure}
\begin{figure}[H]
\caption{Schéma d'une \textit{bank} et d'une cellule. Source : \protect\cite{rowhammer}}
\end{figure}
Une puce contient une grille à deux dimensions dont les intersections entre les lignes et les colonnes sont des cellules qui correspondent chacune à un bit, chaque cellule contient un condensateur. La valeur binaire de la cellule correspond à la charge du condensateur : elle vaut 1 s'il est chargé, 0 sinon. Les lignes de la grille sont des cables que l'on appelle \textit{wordline} et les colonnes \textit{bitline}. Lorsque l'on élève la tension du \textit{wordline} d'une ligne, les valeur des cellules sont transmises au \textit{row-buffer}, cela a pour effet de décharger les cellules de la ligne, pour éviter l'effacement des valeurs, le \textit{row-buffer} rend immédiatement sa valeur à la ligne. Ainsi, lorsque l'on souhaite accéder à une ligne, on augmente la tension du \textit{wordline} correspondant, puis on récupère le résultat inscrit dans le \textit{row-buffer}, il est ensuite envoyé au processeur via le \textit{data-bus} où des opérations pourront être effectuées. Lorsque l'accès est terminé, la valeur du \textit{row-buffer} est inscrite dans la ligne, on diminue la tension du \textit{wordline} et on vide le contenu du \textit{row-buffer}. Un ensemble de ligne est appelé \textit{bank}, chacune des \textit{bank} a un \textit{row-buffer}, et c'est un ensemble de \textit{bank} que l'on appelle \textit{rank}.
\begin{figure}[H]
\centering
\includegraphics[scale=0.4]{images/memctrl-bus-rank-bank.png}
\end{figure}
\begin{figure}[H]
\caption{Accès mémoire : le \textit{memory controller} échange avec un \textit{rank} de 2Go à travers le \textit{data-bus}.\protect\cite{rowhammer}}
\end{figure}
C'est le \textit{memory controller} qui se charge de dialoguer avec le \textit{rank}. Il faut savoir aussi que les valeurs contenues dans une ligne ne sont pas pérennes, si aucun accès à une certaine ligne n'est fait, les condensateurs de ses cellules vont se décharger, c'est pour cela que le \textit{memory controller} peut utiliser la commande \verb=REFRESH= qui correspond à faire un accès à une ligne pour recharger leurs condensateurs. En moyenne une cellule perd sa charge en 64 milliseconde, c'est cette vulnérabilité qu'utilise Rowhammer.
\subsection{Rowhammer}
On remarque en effet que tenter d'accéder de \textbf{nombreuses} fois à la \textbf{même} ligne accélère la décharge des cellules, ainsi certaines valeurs ne seront pas rafraîchies avant d'avoir perdu leur charge, ce qui conduira finalement à une corruption de la mémoire, pouvant provoquer des erreurs de fonctionnement.
Voici comment est implémenté cette corruption de mémoire:
\begin{lstlisting}[language=C,frame=tlrb]
1:
  mov eax, [X]
  mov ebx, [Y]
  clflush [X]
  clflush [Y]
  mfence
  jmp 1
\end{lstlisting}
Explications : On fait deux accès à la RAM aux addresse X et Y, de tels accès provoquent une mise en cache de la valeur récupérée, on utilise donc l'instruction \verb=clflush= pour retirer ces valeurs de nos caches, \verb=mfence= nous garantit que les caches ont été vidés avant la suite du code, puis on recommence, en boucle. Il est important que les addresses X et Y correspondent à des lignes distinctes de la même \textit{bank}, ainsi le \textit{memory controller} va devoir, sans cesse, ouvrir et fermer les accès à ces ligne. Pour que l'attaque fonctionne on est obligé de prendre plus d'une ligne, en effet lors d'accès répétés à une ligne, le memory controller optimise le nombre de commande en ouvrant/fermant qu'une seule fois la ligne ciblée, ce qui annule totalement l'attaque.
Ce code à été implémenté par les auteurs de \cite{rowhammer}, et voici les résultats qu'ils ont obtenus :
descriptions de l'expérience : ce code a été utilisé sur un \textit{rank} de 2 Go, chacune des lignes est accédée des millions de fois.
\begin{figure}[H]
\centering
\includegraphics[scale=0.4]{images/results.png}
\end{figure}
\begin{table}[H]
\caption{Résultats de l'expérience sur la mémoire en fonction du processeur. Source : \protect\cite{rowhammer}}
\end{table}
\subsection{Protection}
\subsubsection{Principe}
Les auteurs de l'article propose un mécanisme de défense simple et efficace contre l'attaque Rowhammer : à chaque fois que l'on ouvre et ferme une ligne il y a une très faible probabilité que l'on fasse un accès à l'une des lignes adjacentes, ainsi accéder de très nombreuses fois à une ligne va presque-sûrement rafraîchir les lignes adjacentes. Cette protection appelée PARA (probabilistic adjacent row activation) est implémentée dans le \textit{memory controller} de la façon suivante : lors d'un accès mémoire, on "lance une pièce non équilibrée", probabilité d'avoir face = \textit{p}, très petite devant 1, lorsque ça arrive, on rafraichit au hasard, avec équiprobabilité, l'une des lignes adjacentes.
\subsubsection{Efficacité}
La technique utilisée étant probabiliste, on ne peut pas garantir la protection la mémoire de manière absolue, néanmoins les résultats de l'expérience montrent que ce système paraît robuste et présente l'avantage d'être simple à embarquer sur les technologies actuelles. Le tableau 2.2 montre les probabilités d'erreurs pour $N_{th}$ accès par intervalle de rafraîchissement avec $p = 0,001$, en fonction de la durée de l'attaque.
\begin{figure}[H]
\centering
\includegraphics[scale=0.4]{images/resultsPARA.png}
\end{figure}
\begin{table}[H]
\caption{Probabilité de corruption de la mémoire lorsque PARA est implémentée.\protect\cite{rowhammer}}
\end{table}
\section{Spectre}
L'attaque Spectre, dévoilée le 3 janvier 2018 par Kocher et al. \cite{spectrepaper}, est une attaque par covert channel exploitant l'exécution spéculative et fonctionnant sur la majorité des processeurs modernes. Bien que le papier originel ne décrive que deux variantes de cette attaque, plusieurs autres ont été découvertes depuis (cf section \ref{ssec:1}). Nous décrivons ici les variantes 1 et 2.

\subsection*{Variante 1: Branchement conditionnel (Spectre-PHT)}\label{spectrepht}
La première variante utilise l'exécution spéculative lors d'un branchement conditionnel d'un processeur. L'idée est de forcer le programme victime à exécuter du code qui normalement ne devrait pas l'être, en abusant des mécanismes de prédictions du processeur. Si ce code executé modifie les caches, l'attaquant peut alors récupérer l'information qu'ils stockent via une attaque Flush+Reload, en bruteforçant une petit zone d'adresses.

Considérons l'exemple de code suivant:
\begin{lstlisting}[language=C,frame=tlrb]
#define CACHE_BLOCK_SIZE 512
unsigned int tab1_size = 16;
uint8_t tab1 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16};
uint8_t tab2[256 * CACHE_BLOCK_SIZE];

/* ... */

/* Dans une fonction */
if (x < tab1_size)
    y = tab2[tab1[x] * CACHE_BLOCK_SIZE];
\end{lstlisting}
Nous avons ici un bound-check pour le tableau \verb=tab1=, puis, si la variable \verb=x= correspond à un index valide de \verb=tab1=, un accès au tableau \verb=tab2=. Nous supposons que l'attaquant a un contrôle sur la variable \verb=x=, et que ni \verb=tab2= ni \verb=tab1_size= ne sont dans le cache. De plus, le contenu de \verb=tab1= doit être dans le cache: dans le cas contraire, le processeur aurait terminé sa spéculation le temps que la valeur de \verb=tab1[x]= ne soit connue. La vérification nécéssitant de connaître la valeur de la variable \verb=tab1_size=, opération pouvant prendre du temps, le processeur va utiliser un de ses mécanismes de prédictions pour ce branchement (ici, le mécanisme employé ici est la PHT). La multiplication par \verb=CACHE_BLOCK_SIZE= permet de nous assurer qu'une seule valeur de \verb=tab2= ne sera accédée dans chaque ligne du cache, afin de ne pas fausser la mesure.

La première partie de l'attaque consiste pour l'attaquant d'être capable d'outrepasser la vérification. Pour ce faire, il "entraîne" la PHT quelques fois sur ce branchement, c'est à dire en répétant ce code plusieurs fois avec un \verb=x= valide. Cet embranchement va ainsi être noté dans les entrées de la PHT comme étant "très souvent pris" \cite{microarchitecture}. Ainsi, au bout de quelques répétitions, le processeur va immédiatement exécuter le code suivant le \verb=if=, puis vérifier que la condition soit vraie avant la fin du traitement de l'opération. Si ce n'est pas le cas, alors le pipeline du processeur est vidé et l'exécution est annulée. Le soucis de cette exécution spéculative est que, bien que le pipeline soit vidé en cas de \verb=x= invalide, la demande de lecture de la variable \verb=tab1[x]= a été lancée et a eu le temps de laisser une trace dans le cache avant que le processeur ne vide le pipeline. Ainsi, en choisissant une variable \verb=x= malicieuse de telle sorte que \verb=*(tab1 + x)= corresponde à un octet secret, le processeur va effectuer une lecture à l'adresse \verb=tab2 + tab1[x] * CACHE_BLOCK_SIZE=, qui est un adresse ne dépendant que de la valeur de \verb=tab1[x]=, lecture qui va être enregistrée dans le cache. Il suffit ensuite à l'attaquant de récupérer la valeur de \verb=tab1[x]= via un covert channel (cf section \ref{sec:canal}) en mesurant les temps d'accès pour chacun des indices de \verb=tab2= pour deviner quelle était la valeur de \verb=tab1[x_malicieux]=, donc de l'octet caché en mémoire.

L'exécution spéculative due au PHT a donc causé une fuite involontaire d'informations, récupérée par l'attaquant via un covert channel.
\subsection*{Variante 2: Empoisonnement de branche (Spectre-BTB)}
Cette seconde variante de Spectre utilise l'exécution spéculative lors de branchements indirects, via les prédictions fournies par le BTB. Contrairement à la variante 1, cette attaque peut s'effectuer sur des espaces d'adressages virtuels différents.

L'idée générale est la suivante: lorsqu'un programme s'exécute, le processeur va stocker dans le BTB les sauts les plus récents, afin de gagner en efficacité. Chaque coeur logique ayant son propre BTB, un attaquant peut effectuer les actions suivantes: un attaquant exécute un programme malicieux sur un coeur, effectuant régulièrement des sauts vers une fonction malicieuse, à l'adresse $A_v$, correspondant à l'adresse de saut $A_s$. Le BTB va donc être rempli avec des sauts vers l'adresse $A_v$. Supposons qu'un programme victime s'exécute sur le même coeur logique. Alors, si le programme victime effectue lui aussi des sauts à l'adresse $A_v$ mais correspondant à l'adresse de saut $B_s$, le processeur mettra dans son pipeline les instructions à l'adresse de saut $A_s$, puis videra son pipeline. L'attaquant peut donc décider de l'endroit où la victime sautera, endroit correspondant à un \textit{spectre gadget}, morceau de code assembleur servant de covert channel pour faire fuiter de l'information sur le programme victime. Un tel gadget doit faire partie soit du code de l'exécutable de la victime, soit d'une librairie partagée entre l'attaquant et la victime. Il peut se présenter sous diverses formes, une proposition de Kocher et al. \cite{spectrepaper} étant, dans le cas où l'information souhaitée est retournée dans une registre $R_1$, de faire sauter le programme dans un gadget mettant la valeur du registre $R_1$ en cache. Bien que cette opération n'affecte pas de manière concrète la mémoire du du programme victime, la valeur souhaitée a été mise en cache et peut être retrouvée par l'attaquant via une attaque de type Flush+Reload.
\begin{lstlisting}[language=C,frame=tlrb]
        Programme attaquant         |          Programme victime
                                    |
0x500   jmp 0x200                   | 0x500    jmp 0x300 => saut vers 0x200 !!
\end{lstlisting}
Le code ci-contre montre le fonctionnement de l'attaque Spectre-BTB. En supposant que le spectre gadget soit situé à l'adresse \verb=0x200=, le programme attaquant va remplir la BTB avec des sauts vers \verb=0x200= lorsque le PC vaut \verb=0x500=. Lors d'un changement de contexte, le processeur exécutera spéculativement le spectre gadget avec le contexte de la victime, forçant le code à sauter à l'adresse \verb=0x200= au lieu de \verb=0x300= avant d'annuler ce saut.

Il est à noter que cette variante est plus difficile à mettre en place que la précédente, dû aux contraintes exposées ci-dessus.
Cette technique est assez semblable au Return Oriented Programming (ROP) \cite{rop}, au sens où elle utilise un gadget déjà présent dans du code compilé et exécuté afin affecter les caches du processeur. A la différence du ROP cependant, l'endroit du saut choisi par l'attaquant ne doit pas nécéssairement se terminer par \verb=ret=, le seul impératif étant d'affecter les caches.
\section{Meltdown}
A l'instar de Spectre, Meltdown est une vulnérabilité hardware que l'on trouve dans les processeurs intel x86, IBM et certains produit ARM \cite{meltdown}. Cette vulnérabilité permet à un programme d'accéder à l'intégralité des mémoires d'une machine.
Cette attaque repose sur deux mécanismes:
\begin{enumerate}

\item \textbf{Exécution d'instructions non permises}: si un programme essaie d'accéder à une zone mémoire interdite (adresse réservée au noyau par exemple), une exception sera levée par la MMU. On va récupérer cette exception et profiter du laps de temps avant l'arrivée du signal pour mettre en cache la valeur accédée

\item \textbf{Construction d'un covert channel} : Une attaque de type Flush+Reload permet de récupérer des informations obtenues.
\end{enumerate}

Nous allons à présent détailler le déroulement de l'attaque. Celle-ci s'effectue en deux parties que nous appellerons "sender" et "receiver", nous détaillons leur rôle. Avant toute chose, nous allouons un bloc de mémoire, que l'on note T, de taille 256*4096 (256 pages mémoires).
\begin{itemize}
  \item Sender : Le rôle du sender est de provoquer une mise en cache de l'information convoitée pour que le receveur puisse la récupérer. Pour commencer on déclenche une interruption, tout le code qui suit ne devrait pas être exécuté, cependant, pour gagner en performance le processeur réorganise l'ordre des instructions d'un programme (cf: Out-Of-Order Execution) cela signifie que les instructions suivantes seront effectuées mais pas issues, il n'y aura donc aucun changement architectural. Nous allons ensuite tenter de lire un octet d'une mémoire inaccessible et on note cet octet 'secret'. On fait un accès à T[secret*4096]. Voici comment cela est mis en oeuvre dans l'attaque Meltdown\cite{meltdown} :
\begin{figure}[H]
\begin{lstlisting}[language=C,frame=tlrb]
; rcx = kernel address, rbx = probe array
xor rax, rax
retry:
mov al, byte [rcx]
shl rax, 0xc
jz retry
mov rbx, qword [rbx + rax]
\end{lstlisting}
\end{figure}
\begin{enumerate}
  \item On lit un octet à l'adresse contenue dans \verb=rcx=, on enregistre cette valeur sur les bits de poids faible de \verb=rax=
  \item On multiplie par 4096 cette valeur
  \item On veut que le résultat soit utilisé comme un index de notre tableau T, comme cette opération ne pourras pas être effectué avant le déclenchement de l'interruption, le résultat ne sera jamais dans \verb=rbx=, mais malgré tout, une page de notre tableau va être mise en cache, c'est tout ce qu'il faut à l'attaquant.
\end{enumerate}
Ligne 6: Les résultats de \cite{meltdown} montre que bien souvent le résultat obtenu lorsqu'on cherche à accéder à une adresse du noyau est 0, cela peut provenir d'un masquage de la mémoire interdite ou une valeur spéculative car les données ne sont pas encore disponibles. La valeur 0 correspond donc à un échec, dans ce cas là, on réessaie avec deux dénouements possibles : on va arriver à lire à l'addresse voulue, ou le signal d'interruption va être reçu par notre programme.
  \item Receiver : Une fois l'information mise en cache nous voulons la lire. Pour cela on utilise une technique de type Flush+Reload : on lance un timer, on accède une case du tableau, on note le temps de réponse, s'il est inferieur à un seuil fixé au préalable, c'est qu'on a évité l'accès à la RAM : on a fait un accès au cache. L'indice correspondant est égal à la valeur secret*4096.
\begin{minipage}{\linewidth}
\begin{lstlisting}[language=C,frame=tlrb]
int i;                                    |static int flush_reload (void *ptr) {
for (i = 0; i < 256; i++) {               |   uint64_t start = 0, end = 0;
  if (flush_reload (T + i * 4096)) {      |   start = rdtsc ();
    if (i >= 1) {                         |   maccess (ptr);
      return i;                           |   end = rdtsc ();
    }                                     |   flush (ptr);
  }                                       |   if (end - start < threshold) {
}                                         |       return 1;
return 0;                                 |   }
                                          |   return 0;
                                          |}
\end{lstlisting}
\end{minipage}
\end{itemize}
Nous pouvons exécuter en boucle cette attaque pour lire l'intégralité de la mémoire de la machine.

\section{Nouvelles attaques}\label{ssec:1}
C. Canella et. al \cite{systematic} ont montré au mois de novembre dernier que les attaques Spectre et Meltdown initiales n'étaient que la partie immergée de l'iceberg des attaques exploitant les mécanismes de prédiction des processeurs. Ils ont ainsi proposé 7 nouvelles variantes de ces attaques et rassemblé celles déjà existantes en un seul document. Ils ont également proposé de les classifier en fonction de l'élément architectural attaqué, classification reprise dans le présent document. Nous détaillons ici les nouvelles variantes montrées dans leur document.

\subsection*{Spectre-STL}
L'élément architectural visé par cette attaque est le mécanisme STL (Store To Load) des processeurs modernes, consistant, lors d'une écriture mémoire, à stocker la valeur dans un tampon, tampon vidé de temps en temps. A chaque chargement mémoire, le processeur parcourt le tampon pour vérifier que la valeur n'a pas des modifications non indexées en mémoire. Les alignements mémoires n'étant pas garantis, le processeur a un mécanisme de prédiction lui permettant de deviner si la case mémoire accédée n'empiète pas sur une autre dans le tampon. Horn \cite{spectrestl} a montré qu'il était possible d'utiliser ce mécanisme, combiné à la technique Flush+Reload, pour faire fuiter certains emplacements mémoire.

\subsection*{Spectre-RSB}\label{sec:spectrersb}
Ici, c'est le RSB (Return Stack Buffer) qui est visé. Ce cache LIFO stocke les dernières adresses des retour empilées après un \verb=call=. Quand un \verb=ret= est exécuté, le sommet de la pile enlevé et utilisé pour donner l'adresse de retour de la fonction, permettant d'accélerer l'exécution d'un programme.  Typiquement, il y a un RSB par coeur logique. Plusieurs chercheurs ont proposé plusieurs variantes de Spectre basés sur ce mécanisme \cite{ret2spec,spectrereturns}. 4 variantes de cette attaque ont été montrées, utilisant soit un sous-remplissage du RSB, forçant le processeur à utiliser le BTB; soit en effectuant sur-remplissage; soit en exploitant les valeurs mises sur le RSB par d'autres threads; ou enfin en mettant spéculativement des valeurs sur le RSB.


Les failles liées à Meltdown sont principalement basée sur le Out Of Order Execution et sur l'exécution spéculative des processeurs modernes.

\subsection*{Meltdown-P (Foreshadow)}
La faille Foreshadow \cite{foreshadow} cible les enclaves SGX d'Intel, technologie permettant l'exécution de code privilégié par un utilisateur à qui on ne fait pas confiance via divers mécanismes de protection, notamment des \textit{enclaves}, zones où l'exécution du code privilégié a lieu. Lorsqu'un accès non autorisé à une enclave a lieu, ce n'est pas une exception \verb=PAGE FAULT (#PF)= qui est levée, ce qui empêche la faille Meltdown classique de fonctionner. Pour contourner cette limitation, l'attaque nécéssite de mettre à zéro le bit "présent" de l'entrée dans la table des pages de la page correspondant à l'endroit où est mappé la clé secrète de l'enclave. Lorsque la victime accèdera à ce secret, une exception \verb=#PF= sera lancée, permettant une attaque Meltdown classique.

\subsection*{Meltdown-GP}
Cette attaque découverte d'abord par AMD (ARM) puis par Intel (x86) est basée sur l'exception \verb= GENERAL PROTECTION (#GP)=, lancée par le processeur lorsqu'un programme essaie d'accéder à des registres privilégiés (ex: les registres MSR (Model Specific Register), propres à chaque modèle de processeur). Le traitement de l'exception se fait comme un Meltdown classique.

\subsection*{Meltdown-NM}
Ici, c'est le changement de contexte qui est ciblé, notamment la sauvegarde des registres FPU. Etant donné la grande taille de ces registres, ils sont simplement marqués "Non disponibles". Lorsque le nouveau programme accède aux FPUs, l'exception \verb=NOT AVAILABLE (#NM)= est lancée par le processeur, permettant à l'OS de sauvegarder les registres FPUs du précédent contexte. Cependant, Stecklina et Prescher \cite{meltdown-nm} ont montré qu'un mécanisme d'exécution spéculative avait lieu, exécution utilisant les registres FPUs du précédent contexte. On peut donc construire un covert channel comme Flush+Reload pour récupérer des informations.

\subsection*{Meltdown-RW}
Kiriansky et Waldspurger \cite{meltdown-rw} ont montré que l'exécution spéculative ne respectait pas les droits Read/Write de la table des pages. On peut donc tenter d'écraser spéculativement des données normalement inaccessibles en écriture, générant une exception \verb=#PF=, et utiliser Meltdown à partir de là.

\subsection*{Meltdown-BR}
L'exception \verb= BOUND RANGE EXCEDEED (#BR)= est une exception hardware lancée par les processeurs lorsqu'un programme tente d'accéder à une case mémoire en dehors d'un tableau. En 32 bits, l'instruction \verb=bound= lance cette exception, tandis qu'en 64 bits c'est l'extension de jeu d'instructions MPX (Memory Protection eXtensions). C. Canella et. al \cite{systematic} on découvert dans leur papier que, de manière similaire à Spectre-PHT (section \ref{spectrepht}), on pouvait récupérer des informations avec Meltdown par l'exécution spéculative suivant l'exception.
