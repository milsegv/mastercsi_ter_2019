\chapter{Mitigations}
Depuis la révélation publique des attaques Spectre et Meltdown, de nombreux efforts ont été déployés afin de se prémunir des effets néfastes de ces dernières. Concernant Meltdown, un correctif de sécurité, nommé \textit{KAISER} (cf section \ref{ssec:kaiser}) permet de supprimer les effets de cette attaque. Pour Spectre, le problème est plus compliqué, étant donné que cette attaque cible des composants précis de l'architecture même du processeur. Cela n'a pas empêché des tentatives de correction de voir le jour, sous la forme de patches logiciels ou sous la forme d'idées pour les futurs concepteurs de processeurs. Nous détaillons d'abord les mitigations mises en place et actuellement déployées, puis les mitigations suggérées, aussi bien de la part des experts en sécurité informatique que des fabricants de microprocesseurs (Intel et AMD).
\section{KAISER}\label{ssec:kaiser}
Le patch KAISER, devenu ensuite KPTI (Kernel Page Table Isolation) sous Linux et KVA Shadow sous Windows, a été présenté théoriquement par D. Gruss \etal \cite{kaiser} en juillet 2017 mais n'a été rééllement déployé sur le noyau Linux que pour la version 4.14. Ce patch, à l'origine pour prévenir des attaques par covert channel sur le hardware, fixe aussi la faille Metldown. Cette section détaille son fonctionnement.

Nous savons que la mémoire virtuelle permet une isolation entre les processus ainsi qu'une isolation entre les processus et le noyau. Lors des opérations sur la mémoire, une traduction est effectuée entre l'adresse virtuelle et l'adresse physique. Au niveau du processeur, un cache tient à jour un journal des appels récents à la RAM pour gagner du temps. La traduction est permise grace à une table de correspondance adresse physique/adresse virtuelle qui est embarquée par chacun des processus dans sa mémoire (la table des pages). Au niveau du processeur, le registre \verb=RC3= contient un pointeur vers cette table des pages, et lors d'un changement de contexte, ce registre est mis à jour avec l'adresse de la nouvelle table. Ainsi, un processus peut accéder aux zones de la mémoires qui sont mappées dans sa table des pages. Il faut également noter que chacune des pages du processus possède certaines propriétes limitant les accès (bits RWX), optimisant la gestion de la mémoire (bit dirty), indiquant sa validité (bit valid), ... .
Comme l'espace d'adressage du noyau est mappé dans l'espace utilisateur, avoir accès aux informations sur la table des pages d'un processus est dangereux, cela permet de construire plus facilement des attaques par covert channel: un attaquant peut essayer d'accéder à une page non accessible, accès qui laissera des traces dans le cache. On veut donc un maximum d'opacité entre l'espace noyau et l'espace utilisateur. Mais comme vu précedemment, les protections mises en places pour tenter de rendre l'espace noyau plus opaque (comme le KASLR, \cf section \ref{sec:aslrkaslr}) sont outrepassée par Meltdown, et par les covert channel de manière générale.
KAISER est un mécanisme résistant aux attaques par canaux auxiliaires qui permet d'avoir une vraie isolation entre l'espace utilisateur et le noyau, basé sur les protections SMEP (Supervisor Mode Execution Prevention) et SMAP (Supervisor Mode Access Prevention): le SMEP empêche l'exécution par le noyau d'instructions stockées dans l'espace utilisateur; tandis que le SMAP étend ce concept à l'accès à de la mémoire utilisateur depuis le noyau. Lorsque le noyau a besoin quand même d'accéder à de la mémoire utilisateur, il peut activer ou non le flag \verb=AC=, responsable de l'activation du SMAP.

Concernant KAISER, chaque processus se voit attribuer deux pages des tables : la première mappe l'espace utilisateur et une très faible partie du noyau, et l'autre mappe, comme auparavant, les pages du noyau et du processus mais avec les protection SMEP et SMAP sur l'espace utilisateur, cette deuxième table des pages n'étant alors utilisée qu'en mode noyau. Lors de l'exécution du processus en espace utilisateur, c'est la première table des pages qu'on utilise, appelée \textit{Shadow Page Table}.
Le premier espace doit contenir le mimimum d'information du noyau afin d'avoir la meilleure isolation possible, il embarque donc la table des interruptions systèmes, quelques informations pour le multithreading, etc... De cette facon, les adresses sensibles du noyau sont complètement invisible en espace utilisateur car elles ne sont pas présentes dans la table des pages.
De plus, l'utilisation du KASLR en plus de KAISER forme une bonne protection contre Meltdown, en effet, l'espace réservée au noyau en espace utilisateur est très faible (quelques kilooctets), la randomisation permet de rendre beaucoup plus difficile de trouver les ces addresses.
\begin{figure}[!h]
\centering
\includegraphics[scale=0.35]{images/kaiser_none.png}
\caption{Mappage du noyau dans la mémoire d'un processus. Source: \cite{kaiser}}
\label{fig:kaisernone}

\bigbreak

\centering
\includegraphics[scale=0.35]{images/kaiser.png}
\caption{Protection KAISER. Source: \cite{kaiser}}

\label{fig:kaiser}
\end{figure}

Les figures \ref{fig:kaisernone} et \ref{fig:kaiser} expliquent schématiquement le fonctionnement du KAISER. Lorsqu'il n'est pas activé, toute la mémoire noyau est mappée dans le processus. Avec le KAISER, une réelle séparation a lieu.


Cette solution possède un inconvénient majeur: étant donné que l'implémentation de KAISER augmente le taux d'appels système, de changements de contexte et de défaut de pages, une baisse des performances est à attendre. Dans le papier originel \cite{kaiser}, les chercheurs donnent une baisse de performance de 0,28\% à 0,68\%. Cependant, un ingénieur en performances a posté sur son blog \cite{kaiserperf} une analyse plus complète, indiquant une perte de performances de pouvant aller de 0,1\% à 6\% dans des cas rééls.

\section{retpoline}
Pour Spectre-RSB, une technique baptisée \textit{retpoline} \cite{retpoline} est une solution efficace à l'exploitation du RSB, implémentée dans les compilateurs. Elle permet de supprimer les fuites à cause de l'exécution spéculative via le RSB en l'emprisonnant dans une boucle infinie. La figure suivante compare un code avant et après le retpoline activé:

\begin{figure}[H]
\noindent
\begin{minipage}{.30\textwidth}
\begin{lstlisting}[caption=Sans retpoline,frame=tlrb]
jmp *%rax
\end{lstlisting}
\end{minipage}\hfill
\begin{minipage}{.45\textwidth}
\begin{lstlisting}[caption=Avec retpoline,frame=tlrb]
    call load_label
capture_ret_spec:
    pause
    lfence
    jmp capture_ret_spec
load_label:
    mov %rax, (%rsp)
    ret
\end{lstlisting}
\end{minipage}
\caption{Comparaison de la technique retpoline \cite{retpoline}}
\end{figure}

Lorsque le code effectue un branchement indirect, l'adresse contenue dans \verb=%rax= est mise sur la pile, puis le programme retourne à la valeur contenue à cette adresse, nécéssitant la résolution du branchement indirect défini par \verb=%rax=. Hors, le sommet de pile du RSB contient \verb=capture_ret_spec=, qui est une boucle infinie forçant la sérialisation des instructions. Le processeur, en spéculant en attendant la résolution du branchement indirect, va donc exécuter le code se trouvant au sommet du RSB, donc la boucle infinie, empêchant l'exécution de code compromettant. Sur GCC, le retpoline est en fait l'instruction \verb=__x86.indirect_thunk= \cite{gccretpoline}, rajoutée début janvier 2018 après la sortie de l'attaque Spectre.

Cette mitigation fonctionne sur les processeurs Intel, AMD et ARM \cite{systematic}

\section{Mitigations par Webkit}
En réponse à la Proof Of Concept de Spectre utilisant le langage Javascript pour lire la mémoire d'une victime, Apple \cite{webkit} a déployé plusieurs mesures sur son moteur Webkit, notamment la réduction de la précision des timers internes de leur moteur Javascript à 1ms, empêchant une timing attack sur les cache de fonctionner.

Une technique pour réduire dynamiquement les accès out-of-bounds des tableaux, proposée par WebKit \cite{webkit}, est le \textit{index masking}, consistant, lors d'un accès à un tableau, à masquer l'indice correspondant avec un \verb=&= logique, empêchant de trop grands débordements. Cette mitigation empêche juste un attaquant de lire à des adresses arbitraires en mémoire, mais n'empêche pas les accès out-of-bounds.

Afin de limiter les accès à des zones non autorisées via des pointeurs (et les abus de vérification des types), WebKit a aussi proposé \cite{webkit} de XORer dynamiquement un bit sur la partie haute du contenu de chaque pointeur avec un \textit{poison}, assurant ainsi qu'un accès à ce pointeur soit dans une zone non mapée. Ce bit est tiré au hasard à la compilation. Ainsi il est nécéssaire d'enlever ce bit rajouté pour utiliser le pointeur.

\begin{figure}[H]
\noindent
\begin{minipage}{.30\textwidth}
\begin{lstlisting}[caption=Sans index poisoning,frame=tlrb]
class Foo : public Base {
public:
    ...
private:
    int m_x;
    Bar* m_y;
};
\end{lstlisting}
\end{minipage}\hfill
\begin{minipage}{.45\textwidth}
\begin{lstlisting}[caption=Avec index poisoning,frame=tlrb]
class Foo : public Base {
public:
    ....
private:
    struct Data {
        int x;
        Bar* y;
    };
    ConstExprPoisoned<FooDataKey, Data> m_data;
};
\end{lstlisting}
\end{minipage}
\caption{Comparaison de la technique index poisoning de Webkit \cite{webkit}}
\end{figure}

Le type \verb=ConstExprPoisoned= est une table de hachage indicée par la clé permettant d'enlever le bit de poison. Au lieu d'accéder aux champs privés de la classe \verb=foo=, on est obligés d'utiliser le champ \verb=m_data= contenant le poison.


\section{Autres solutions pour Spectre}
\begin{itemize}
\item \textbf{Concernant l'exécution spéculative}: Incorporer un mode dans le processeur permettant de complètement désactiver l'exécution spéculative. Un programme pourrait ainsi, lors d'opérations critiques, diminuer un peu ses performances au profit d'une sécurité améliorée.

Une autre mitigation serait de forcer la sérialisation des instructions avant et après des opérations de branchement, via l'instruction \verb=lfence= \cite{intelwhitepaper}. Le code pourrait être analysé statiquement par le compilateur pour trouver les endroits vulnérables du code où insérer ces instructions. Un autre moyen serait pour le développeur d'indiquer dans son code les sections critiques où la spéculation ne devrait pas avoir lieu. Dans tout les cas, la perte de performance est assez élevée.

\item \textbf{Limiter l'accès à des zones non autorisées}:
Une proposition pour limiter les accès à des zones non autorisées appelée "You Shall Not Bypass" \cite{youshallnot} exploite le fait que les processeurs contiennent un mécanisme permettant de trouver les dépendances de données entre instructions. Ainsi, la proposition est d'étendre cette dépendance aux comparaisons, les instructions suivant une instruction seraient alors forcés d'attendre que la comparaison soit résolue avant d'être exécutées.

Afin de se prémunir contre certaines variantes Spectre-RSB (section \ref{sec:spectrersb}), Intel a proposé de remplir le RSB avec des valeurs aléatoires.

\item \textbf{Limiter l'extraction par covert channels}:
Les solutions SafeSpec \cite{safespec} et InvisiSpec \cite{invisispec} permettent de gérer les extractions par les caches en proposant des structures hardware supplémentaires, responsables de tous les chargements mémoires spéculatifs. Ainsi, lorsqu'une prédiction est effectuée, la valeur mémoire chargée est mise dans ce dispositif supplémentaire, et est mis dans le réél système de cache lorsque la prédiction est correcte. Dans le cas contraire, la valeur est détruite. C'est en fait une obfuscation de l'effet de bord provoqué par l'exécution spéculative.

Y. Yarom et K. Falkner \cite{flushreload} proposent de restreindre l'utilisation de l'instruction \verb=clflush= à des zones mémoires dont l'utilisateur a des permissions d'écritures, mais aussi à des zones mémoires explicitement autorisées par le système.

\item \textbf{Empêcher l'empoisonnement de branche (Spectre-BTB)}: Intel et AMD ont proposé \cite{spectrepaper} plusieurs ajout à l'ISA x86 restreignant la prédiction des branchements: la première fait une distinction entre branchements effectués par l'utilisateur et ceux effectués par le noyau; la seconde empêche de partager les prédictions entre les processus s'exécutant sur le même hyperthread (pour Intel).

La différence de performance des ces mitigations oscille entre quelques pourcents et une facteur x4 de performances en moins, ce qui n'est absolument pas négligeable.

Pour ARM, quelques processeurs sont compatibles avec un mécanisme de protection invalidant la prédiction de branche \cite{systematic}.
\end{itemize}
