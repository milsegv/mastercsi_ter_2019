\chapter{Généralités et Vocabulaire}
Afin d'assurer au lecteur une bonne compréhension des attaques présentées dans ce document, cette section introduit les bases du fonctionnement d'un processeur moderne, notamment les deux mécanismes principaux sur lesquels reposent les attaques Spectre et Meltdown: l'exécution spéculative; l'execution dans le désordre ; et le fonctionnement de base des caches hardware.

\section{Pipeline et exécution spéculative}\label{sec:pipeline}
Le pipeline d'un processeur, dont la taille dépend de l'architecture, est la pierre angulaire des processeurs modernes. Il est responsable de l'exécution étape par étape des instructions.
Quand une instruction assembleur est lue par le processeur, ce dernier découpe cette instruction en \textit{micro-opérations}, représentée par des $\mu OP$ \cite{microarchitecture} (qui correspondent à la traduction de l'instruction en un langage compréhensible par le processeur), décrivant une opération basique du processeur: addition, lecture mémoire, etc... . L'instruction découpée passe alors par plusieurs \textit{units} (unités en français), chaque étape correspondant à une micro-opération. De façon analogue à un pipeline de fabrication de voitures, les $\mu OPs$ doivent passer par l'intégralité des units avant que l'instruction ASM ne soit considérée comme terminée. Ce fonctionnement permet au processeur d'optimiser son efficacité : il peut insérer les instructions les unes à la suite des autres, sans attendre que la première soit terminée avant d'insérer la seconde.

Néanmoins, ce fonctionnement est parfois limité lorsque la micro-opération correspond à un branchement conditionnel, le processeur n'étant pas capable pas deviner quelle va être la bonne action a exécuter avant que la condition de saut ne soit résolue, résolution pouvant mettre un grand nombre de cycles à se terminer (si la variable est dans la RAM par exemple). Afin de ne pas gaspiller des cycles d'horloge, les architectes ont optimisé les pipelines via de nombreuses techniques. Un de ces mécanismes se nomme \textit{exécution spéculative} : lors d'un branchement conditionnel, le processeur va quand même faire progresser la micro-opération dans son pipeline en remplaçant la valeur demandée par une des valeurs/adresses de saut probables (\cf section \ref{branchement_opti}). En même temps, il va chercher la valeur/l'adresse en mémoire, déterminant la réelle valeur de la variable/du saut. Une fois la valeur récupérée, soit le processeur vide le pipeline si les instructions exécutées étaient mauvaises, soit effectivement il affecte de manière permanente le système (\ie les registres ou la mémoire). Plusieurs mécanismes ont été mis en place afin d'améliorer la précision de la prédiction et surtout de diminuer la fréquence des mauvaises prédictions lors d'un branchement conditionnel. Elles sont détaillées en section \ref{branchement_opti}.

\section{Exécution dans le désordre}
Tous les processeurs Intel depuis la sixième génération \cite{microarchitecture} embarquent un changement important au niveau de la microarchitecture que l'on appelle \textit{Out-of-Order execution}, ou exécution dans le désordre. L'idée est la suivante : lorsqu'une instruction ne dispose pas d'une de ses entrées (parcequ'elle dépend d'une variable stockée en RAM par exemple), le microprocesseur va essayer d'effectuer d'autres instructions censées arriver plus tard dans le programme qui ont, elles, leurs entrées prêtes. Si les instructions qui suivent ont besoin de la sortie de l'instruction mise en attente, le microprocesseur ne peut pas utiliser cette technique, une telle suite d'instruction est appelée \textit{dependency chain}. On veut au maximum éviter cela, car moins les instructions dépendent des précédentes, plus le processeur pourra utiliser l'exécution dans le désordre.
Pour limiter la dépendence, on va découper les instructions en micro-opérations, comme indiqué section \ref{sec:pipeline}. Prenons deux micro-opérations d'une instruction, si aucune des deux ne prend en entrée la sortie de l'autre et qu'elles utilisent des \textit{units} différentes alors le microprocesseur peut effectuer ces deux micro-opérations en parallèle.

Prenons un exemple simple : lorsqu'on effectue un \verb=push eax=, on peut imaginer le séparer en deux micro-opérations :
\begin{verbatim}
  sub esp, 4
  mov [esp], eax
\end{verbatim}
L'avantage de cette méthode est que la première instruction peut toujours être effectuée, elle n'a besoin d'aucune autre donnée, on peut donc la traiter plus tôt dans l'exécution du programme.
Autre exemple \cite{microarchitecture} :
\begin{verbatim}
  mov eax, [mem1]
  imul eax, 6
  mov [mem2], eax
  mov eax, [mem3]
  add eax, 2
  mov [mem4], eax
\end{verbatim}
Ce bout de code effectue deux choses différentes : il lit une case en mémoire, multiplie son contenu par 6 et replace le résultat en mémoire, puis il lit une case en mémoire, ajoute 2 à la valeur contenue et place le résultat en mémoire. Mais ici tout se fait dans le registre \verb=eax= ce qui devrait obliger le processeur à faire les calculs les uns après les autres, alors qu'ils ne sont pas dépendants et utilisent des unités différentes : on pourrait les faire en parallèle. Le processeur va en réalité utiliser un registre temporaire pour les trois dernières instructions. Il y a cependant un problème : lors de l'opération \verb=mov [mem4], eax=, comment le processeur sait-il s'il doit utiliser le registre temporaire ou le registre \verb=eax= ?
Dans les faits, le processeur va "renommer" les registres, c'est à dire que le registre temporaire sera considéré comme étant \verb=eax= pour un moment, ainsi l'exécution n'est pas altérée par la mise en parallèle des opérations.
Voici comment fonctionne ce mécanisme qu'on appelle \textit{Register Renaming}: à chaque fois que l'on écrit ou modifie une valeur de l'un des registres logiques, le microprocesseur assigne un nouveau registre temporaire au registre en question, le temporaire va contenir la sortie de l'opération tandis que le registre logique contiendra l'entrée.
L'avantage à cette multiplicité des registres est que nous pouvons mettre en oeuvre notre exécution dans le désordre. Prenons un autre exemple tiré de \cite{microarchitecture}.
\begin{verbatim}
  mov eax, [mem1]
  mov ebx, [mem2]
  add ebx, eax
  imul eax, 6
  mov [mem3], eax
  mov [mem4], ebx
\end{verbatim}
On suppose que \verb=[mem1]= est en cache et \verb=[mem2]= non. On peut donc traiter la multiplication avant l'addition, l'avantage d'utiliser un registre temporaire pour le résultat de la multiplication est que la valeur stockée dans \verb=eax= est toujours celle de \verb=[mem1]= et on peut effectuer l'addition lorsque le registre \verb=ebx= sera prêt, sans devoir aller rechercher dans le cache la valeur de \verb=[mem1]=.

Globalement, le fonctionnement de l'Out-Of-Order execution est le suivant:
Le processeur reçoit un flux d'instructions, il les sépare en micro-opérations qu'il peut exécuter en parallèle. Ces micro-opérations entrent dans des \textit{reservation station}, ou elles vont patienter jusqu'à obtenir leurs paramètres d'entrée, dès que ceux-ci sont obtenus, les micro-opérations quitte la file et font les calculs, les résultats sont mise dans une nouvelle file d'attente. Une fois que tous les résultats sont bien arrivés, ils quittent la file dans l'ordre d'exécution.
\section{Caches}
Les caches hardware, implémentés dans la majorité des processeurs depuis les années 90, sont des petites mémoires rapides proches du processeur permettant de stocker des données récemment et/ou fréquemment utilisées. Ainsi, lorsqu'un programme accède pour la première fois à une donnée stockée en RAM, cette donnée est copiée dans le cache, accélérant ainsi les accès ultérieurs. Ces éléments occupant une place centrale dans les attaques hardware détaillées dans ce document, il est indispensable d'en comprendre le fonctionnement de base. Pour faciliter la compréhension, nous nous concentrerons uniquement sur l'architecture des caches utilisés dans les processeurs intel.

Intel utilise trois niveaux de caches dans ses processeurs. Quand une donnée est demandée par un programme, le processeur va parcourir chacun des niveaux de cache puis la RAM pour retrouver la donnée. Plus la donnée est présente dans un cache/une mémoire éloignée physiquement du processeur, plus le temps de récupération de cette donnée sera longue. Les trois types de cache utilisés par Intel sont présentés ci-dessous:
\begin{itemize}
\item \textbf{L1d}: Cache de plus bas niveau, stockant les données. Est spécifique au coeur considéré.
\item \textbf{L1i}: Cache de plus bas niveau, stockant des séquences d'instructions décodées. Est spécifique au coeur considéré.
\item \textbf{L2}: Cache de niveau intermédiaire, uniquement pour les données. Est spécifique au coeur considéré.
\item \textbf{L3 (ou LLC, Last Level Cache)}: Cache de plus haut niveau, uniquement pour les données. \textbf{Est partagé entre tous les coeurs d'un processeur.}
\end{itemize}

Les caches Intel sont \textit{inclusifs}, signifiant que si une variable est présente dans l'un des niveaux de caches elle sera copiée dans les niveaux inférieurs via un algorithme d'inclusion.

Le tableau \ref{tab:caches} décrit les différences de performances en fonction du niveau de cache considéré.
\begin{table}[h]
\centering
\begin{tabular}{|c|c|c|c|}
\hline
Niveau de cache & Latence & Nombre de cycles CPU& Taille\\
\hline
L1 & 1 ns & 4 & 32 à 64 Kb \\
\hline
L2 & 3 à 10 ns & 12 à 40 & 256 Kb à 1 Mb \\
\hline
L3 & 10 à 20 ns & 40 à 80 & 16 à 64 Mb \\
\hline
RAM  & 50 à 100 ns & 200 à 400 & 16 à 256 Gb \\
\hline
\end{tabular}
\caption{Temps de récupération moyen d'une donnée sur un processeur moderne \cite{cachesizes}}
\label{tab:caches}
\end{table}

On note une augmentation considérable de l'efficacité de récupération d'une instruction par rapport à la mémoire RAM, les caches sont donc indispensables au fonctionnement efficace de nos processeurs modernes.

Un dernier point de vocabulaire sur les caches: lorsqu'une valeur demandée est stockée dans le cache, on parle de \textit{cache hit}, le temps d'accès à la valeur est alors très court. Dans le cas contraire, on parle de \textit{cache miss}.

\subsection*{Instructions assembleur sur le cache}
Les attaques décrites dans le présent document nécessitent d'introduire dès maintenant les instructions x86 \verb=clflush=, \verb=mfence= et \verb=lfence=.

La première, \verb=clflush= pour \textit{Cache Line FLUSH}, permet de vider une ligne spécifique de tous les niveaux des caches d'un processeur. Cette instruction nous servira à effectuer des timing attacks (voir la section \ref{sec:flushreload}).

Les suivantes, \verb=mfence= pour \textit{Memory FENCE} et \verb=lfence= pour \textit{Load FENCE}, sont des \textit{barrières} permettant de forcer la sérialisation des instructions en attente \cite{mfence, lfence, flushreload}. \verb=mfence= force la sérialisation de toutes les lectures et écritures en mémoire, des autres barrières  et de \verb=clflush= et garantit cette sérialisation avant toute lecture/écriture. Cependant, il n'est pas bloquant, contrairement à  \verb=lfence= qui ne force que les lectures depuis la mémoire mais qui est bloquant. La combinaison de ces deux instructions permet donc de s'assurer du bon comportement du programme attaquant; sans elles, un processeur pourrait réordonner les instructions ou les délayer.

\section{Branchements et optimisations} \label{branchement_opti}
\subsection*{Conditionnel - direct - indirect}
Dans un processeur, un \textit{branchement} est un endroit dans le programme où le code exécuté effectue un saut à une adresse choisie. On parle de \textit{branchement conditionnel} quand le saut est soumis à une condition. Par exemple, l'instruction \verb=jle L0= effectuera un saut à l'adresse correspondant à l'étiquette \verb=L0= si le flag Z est positionné à 1 ou si le flag S est positionné à 1.

Il est aussi important d'aborder la notion de \textit{branchement direct} et de \textit{branchement indirect}. Un branchement direct est un saut dont l'adresse considérée est statique et déterminée à la compilation. Inversement, un branchement indirect est un saut dont l'adresse considérée est variable, par exemple contenue dans un registre (ex: \verb=jmp *%rax=)

\subsection*{Branch Target Buffer (BTB)}
Une de ces optimisations est le \textit{Branch Target Buffer} \cite{microarchitecture}, un cache situé dans chaque coeur logique stockant toutes les adresses des sauts effectués. L'adresse destination est stockée dans le cache lorsqu'un saut non conditionnel est executé pour la première fois, ainsi que la première fois qu'un branchement conditionnel est pris. Quand un programme refait le même saut, le processeur utilise l'adresse stockée dans le BTB et ne vérifie que son choix était le bon qu'une fois que l'instruction est arrivée à l'étape d'exécution du pipeline.

Etant donné qu'en fonction du processeur considéré, l'organisation du BTB change, s'optimise et s'améliore; nous ne sommes pas en mesure de donner une description exhaustive du BTB. Néanmoins, nous allons prendre comme exemple le BTB utilisé sur les processeurs Pentium II et III. C'est un ancien modèle qui nous permettra de comprendre le fonctionnement global du BTB.

Le BTB est en pratique une table (voir figure \ref{fig:btb}) dont chacune de ses entrées est composée des 30 bits les plus bas de l'adresse d'une adresse d'une instruction de branchement \cite{btbexplain} (30 bits étant pour le Pentium II et III). Lorsque le PC correspond à l'adresse d'un branchement, le processeur regarde dans le BTB l'adresse de saut correspondant au PC actuel et saute à l'adresse trouvée.

\begin{figure}[h]
\centering

\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,semithick]
\node[draw,black] (A) at (0,0) {\begin{tabular}{l}
PC \\ \hline
0x5555555554c8
\end{tabular}};

\node[draw,black] (B) [right = 2cm of A]{\begin{tabular}{|l|l|}
    \hline
    Adresse du branchement & Adresse du saut \\
    \hline
    0x5555555555b2 & 0x1719 \\
    \hline
    0x5555555554c8 & 0x20ab \\
    \hline
    0x5555555556ff & 0x21ae \\
    \hline
\end{tabular}};

\node[draw,black] (C) [right = 1.5cm of B] {\begin{tabular}{l}
0x20ab
\end{tabular}
};

\path (A) edge node {Recherche} (B)
(B) edge node {Sortie} (C);
\end{tikzpicture}

\caption{Fonctionnement du BTB} \label{fig:btb}
\end{figure}

\subsection*{Branch History Register et Pattern History Table (PHT)}
Pour les sauts conditionnels, le processeur doit    prédire, en plus de l'adresse du saut, si il sera effectué ou non. Cette prédiction est effectuée par la \textit{Pattern History Table} (PHT) en duo avec le \textit{Branch History Register} (BHR) \cite{microarchitecture}.

Le BHR est un compteur à décalage à gauche sur n bits (ici: 2 bits) qui note l'historique d'un branchement. Ainsi, si un branchement est pris au premier saut mais pas au second, il contiendra la valeur \verb=10=. Si le branchement est repris à l'itération suivante, il contiendra, après décalage à gauche, la valeur \verb=01=.

La PHT quant à elle est une table prédisant pour chacune des valeurs du BHR possibles quelle sera l'issue la plus probable de la branche. Elle est indicée par les $2^n$ valeurs possibles du BHR, et contient pour chaque entrée une machine à états (ici: à 4 états, cf figure \ref{fig:phtentry}). Chaque état indique la fréquence d'utilisation de la branche. Lorsque la machine est dans un de ses 2 états les plus bas, la PHT prédit que le branchement ne sera pas pris. Inversement, si la machine est dans un de ses 2 états les plus hauts, la PHT prédit que le branchement sera pris. Lorsque le programme est effectivement exécuté par le processeur, la PHT est mise à jour en fonction de l'issue du saut: si le saut a effectivement été effectué, la machine correspondant à l'entrée considérée progresse vers un état supérieur, et vers un état inférieur si ce n'était pas le cas.

\begin{figure}[!htp]
\centering
\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,semithick]
  \tikzstyle{every state}=[fill=white,draw=black,text=black, text width=2cm, align=center,  scale=0.8]

  \node[state]          (A)                    {\small{Très rarement pris}};
  \node[state]         (B) [right = 1cm of A] {\small{Rarement pris}};
  \node[state]         (C) [right = 1 cm of B] {\small{Souvent pris}};
  \node[state]         (D) [right = 1 cm of C] {\small{Très souvent pris}};

  \path
  (A) edge [bend left] node {Pris} (B)
  (A) edge [loop left] node {Non pris} (A)

  (B) edge [bend left] node {Pris} (C)
  (B) edge [bend left] node {Non pris} (A)

  (C) edge  [bend left] node {Pris} (D)
  (C) edge [bend left] node {Non pris} (B)

  (D) edge [loop right] node {Pris} (D)
  (D) edge [bend left] node {Non pris} (C);
\end{tikzpicture}
\caption{Une entrée de la PHT: une machine à état} \label{fig:phtentry}
\end{figure}


Il faut cependant noter que cette solution est très simple car coûteuse en espace, augmentant exponentiellement en fonction de la taille du BHR. De nombreux autres mécanismes entrent en jeu dans les processeurs modernes, comme la Global History Table, mais nous ne les détaillerons pas ici. Ces mécanismes sont recensés dans le document \cite{microarchitecture}.


\section{ASLR, KASLR}\label{sec:aslrkaslr}
L'ASLR (Address-Space Layout Randomisation) est une technique de protection mise en place afin de prévenir les corruptions de mémoire. L'idée est la suivante: lors de la création d'un processus, une image en mémoire est crée, découpée en section: on y trouve la pile, le tas, le code, des fonctions de la libc, etc.
Si un attaquant parvient à trouver une vulnérabilité dans le programme et change l'adresse du pointeur d'instruction il peut altérer l'exécution du programme : par exemple en faisant un appel à la fonction \verb=system()= de la libc.
En changeant de manière aléatoire les positions de la pile, du tas, de la libc, et de toutes les zones critiques de la mémoire, à chaque exécution du programme, l'attaque devient moins consistante.
L'ASLR ne randomise pas certaines zones de la mémoire, le code en est un exemple. Mais plus la zone de la mémoire aléatoirement distribuée est importante, plus il devient difficile d'attaquer le programme, à condition qu'on ne puisse pas faire fuiter d'information.
Dans les systèmes Linux, on a trois niveau d'ASLR :
\begin{itemize}
 \item 0 : Pas de randomisation
 \item 1 : Distribution aléatoire de la pile; la section vDSO (virtual Dynamic Shared Object) qui est un ensemble de routines du noyau fréquemment utilisées mises à disposition dans l'espace utilisateur afin d'éviter les changements de contexte trop récurrents; et les mémoire partagées (comme la libc).
 \item 2 : Mêmes ensemble qu'au niveau 1, mais on distribue aussi aléatoirement le segment \verb=data= du processus.
\end{itemize}
C'est le niveau 2 qui est activé par défaut.
Une autre des implémentations semblables à l'ASLR s'appelle le KASLR (Kernel Address-Space Layout Randomization), randomisant l'espace d'adressage du noyau au démarrage: de la même manière que les fichiers binaires peuvent être attaqués, on peut exploiter les vulnérabilités du noyau; or, le mode noyau étant privilégié, une attaque peut permettre de s'octroyer les droits root sur une machine. La protection KASLR permet de rendre imprévisible les adresses du noyau et ainsi rendre difficile voire impossible certaines attaques contre le kernel.
