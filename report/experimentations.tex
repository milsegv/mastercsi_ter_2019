\chapter{Améliorations, Expérimentations}
Nos expérimentations ont principalement porté sur la mise en pratique des attaques présentées chapitre \ref{chapter:failles}. Ainsi, nous avons implémenté Flush+Reload, et effectué des mesures de performances des attaques Spectre-PHT et Meltdown selon différents paramètres.

\section{Flush+Reload}
Nous avons implémenté une version simplifiée de l'attaque Flush+Reload, via une libraire partagée fait main, \verb=libmy_shared.so=. Cette librairie contient un tableau global, ainsi qu'une fonction incrémentant l'un des éléments du tableau (ici, l'élément contenu à l'indice 50).

Un programme victime qui ne fait qu'exécuter cette fonction est lancé et est observé par un programme attaquant, qui lui mesure les temps d'accès à la case 50 du tableau partagé. Si le temps d'accès est inférieur à une limite (\verb=THRESHOLD=), le programme considère que la variable était bien dans le cache.

Cette PoC très simple censée vérifier le bon fonctionnement de l'attaque Flush+Reload, a posé plusieurs soucis, résumés ci-dessous:
\begin{itemize}
\item Lorsque le programme attaquant parcourt toutes les cases possibles du tableau (et non plus seulement la case 50), les résultats affichés ne correspondent pas à la case du tableau réellement accédée par la victime (la case 50). Cette anomalie est sans doute dûe à la façon dont le processeur charge le tableau en cache: au lieu de ne charger que la case considérée, il  essaie de charger tout le tableau afin d'éviter les défauts de cache.
\item Lorsque le programme victime incrémente aléatoirement des éléments du tableau, le même phénomène est observé, sans doute pour les même raisons.
\item En insérant des \verb=lfence= ou des \verb=mfence= dans la portion de code s'occupant du flush, nos résultats varient énormément, allant de ne plus du tout détecter l'accès à la le détecter tout le temps.
\end{itemize}

Notre implémentation ne confirme pas le fonctionnement systématique de l'attaque: elle ne détecte pas tout les accès mémoires, mais lorsque le programme victime n'accède à aucune case le programme ne détecte pas d'accès. Les accès détectés sont épars, nous supposons que cela est dû à des mécanismes d'optimisations que nous ignorons.

La difficulté rencontrée dans l'implémentation de cette PoC est principalement la détermination de \verb=THRESHOLD=. Cette limite dépendant de l'architecture sur laquelle le programme est exécuté, il nous a fallu en tester plusieurs afin de calculer une limite varaible. Sur un Laptop Intel Core i3-2350M, nous l'avons fixée à 190 cycles CPU.

\newpage
\section{Meltdown}\label{exp-meltdown}
Nous avons mesuré le temps de lecture de la mémoire avec l'attaque Meltdown, la machine victime est équipée d'un Intel Core i3-2350M 2.3GHz. L'attaque n'embarque aucune optimisation de notre part ou de la part des chercheurs, le nombre d'octets lu varie entre 32 et 4608, avec un pas de 32 octets.
\begin{figure}[H]
\centering
    \includegraphics[scale=0.8]{images/graph_meltdown.png}
    \caption{Performance de l'attaque Meltdown - équipement : Intel Core i3-2350M 2.3GHz}
    \label{fig:meltdown-perf}
\end{figure}
Le résultat présenté figure \ref{fig:meltdown-perf} est clairement linéaire en fonction de la taille lue.

Le seul défaut de notre mesure se trouve au niveau de la vitesse moyenne : nous obtenons finalement une vitesse moyenne de 244 octets/s, un résultat bien plus faible que les 512Ko/s annoncés \cite{meltdown}. Nous mettons cela sur le compte de l'absence d'optimisation et de l'importante différence de performances entre la machine utilisé par les chercheurs et la nôtre. Nous n'excluons pas une mauvaise implémentation des mesures, générant un débit aussi faible.

\newpage
\section{Spectre-PHT}
\begin{figure}[!h]
\centering
\includegraphics[scale=0.8]{images/spectre-pht-perfs.png}
\caption{Performances de l'attaque Spectre-PHT en fonction du type d'équipement testé - Intel Core i3-2350M 2.3GHz}\label{fig:spectre-expe1}
\bigbreak
\centering
\includegraphics[scale=0.8]{images/spectre-pht-perfs-tour.png}
\caption{Performances de l'attaque Spectre-PHT en fonction du type d'équipement testé - AMD Ryzen 5 1600 3.2GHz}\label{fig:spectre-expe2}
\end{figure}\label{fig:spectre-expe}
\newpage

L'expérience a consisté à lire un certain nombre d'octets en utilisant l'attaque Spectre. Le débit de lecture est obtenu en divisant la taille lue par le temps de lecture. Les figures \ref{fig:spectre-expe1} et \ref{fig:spectre-expe2} représentent les résultats obtenus sur deux processeurs différents. La VM utilisée est une VirtualBox, avec VT-X activé (obligatoire pour les systèmes 64 bits). Nous avons comparé les performances de l'attaque Spectre-PHT avec KVM, sans KVM et hors VM. Nous avons effectué 1000 mesures pour chacune des tailles lues, réparties de 32 en 32 entre 32 et 638 octets, afin d'éviter les phénomènes d'optimisation locaux que nous n'aurions sur prédire. Malgré ces précautions, les résultats obtenus figure \ref{fig:spectre-expe1} sont erratiques. On peut quand même, en la comparant avec la figure \ref{fig:spectre-expe2}, interpréter les résultats.

La différence entre les deux graphes est significative. Sur la figure \ref{fig:spectre-expe1}, la marge d'erreur est bien plus élevée, cela est sans doute dû à l'ancienneté et à la mauvaise performance du matériel testé. Néanmoins, l'allure des résultats est relativement semblable entre les deux machines: dans une VM, on a des résultats relativement constants sauf pour les petites tailles. La différence KVM/sans KVM est également notable, bien que leurs allures soient similaires. C'est hors VM que les courbes se différencient: sur le i3-2350M, la courbe a une allure plus ou moins croissante, tandis que l'allure logarithmique apparaît clairement sur le Ryzen 5 1600.

La grosse remarque à faire est la différence de performance entre un environnement virtualisé et un environnement non virtualisé. Nous n'avons pas d'explication certaine pour ce phénomène, mais il pourrait être dû à la différence de traitement par le processeur des instructions venant d'une VM par rapport à celles venant de la machine hôte. Il se pourrait que des mécanismes d'optimisations qui nous sont inconnus entrent en jeu.

Au niveau de la différence de performance, lorsque les tailles lues augmentent, les performances hors VM rejoignent celle de la VM. Encore une fois nous n'avons pas d'explication pour ce phénomène.

Sur le papier originel de Spectre \cite{spectrepaper}, les chercheurs ont observé un débit de 10Ko/s sur un Intel Core i7-4650U. Nous avons utilisé la PoC fournie avec leur papier, que nous avons légèrement modifiée afin de pouvoir mesurer des temps d'exécution. Malgré cela, leurs performances sont diffèrent des nôtres. Selon \url{cpubenchmark.net}, le processeur i3-2350M est un peu moins puissant que le i7-4650U, tandis que le Ryzen 5 1600 est beaucoup plus performant. Malgré cela, nous n'arrivons pas à obtenir le même débit que les chercheurs. Cela est sans doute dû à des optimisations intégrées à leur code, mais non rendues publiques pour des raisons de sécurité.

Nous avons aussi tenté de transformer la PoC originale de Spectre en une version 32 bits, en adaptant simplement les instructions assembleur en utilisant le \verb=__asm__= de GNU, mais sans succès.
