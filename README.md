# Université de Bordeaux - Master 1 CSI - TER

Auteurs: Bérenger Faurot & Emile Josso

## From RowHammer to Spectre and Meltdown : Recent Hardware Flaws

Le but de ce projet est de faire un historique et une synthèse des récentes
failles matérielles trouvées sur les systèmes de caches et les accès mémoires
spéculatifs au sein des processeurs.

## Références
- A Systematic Evaluation of Transcient Execution Attacks and Defenses, by Claudio Canella et al., Nov 2018 (arXiv).
- Intel Analysis of Speculative Execution Side Channels (White Paper), Intel Corp., July 2018.
- Meltdown : Reading Kernel Memory from User Space, by Moritz Lipp et al., USENIX Security, August 2018.
- Spectre Attacks : Exploiting Speculative Execution, by Paul Kocher et al., January 2018.
- KASLR is Dead : Long Live KASLR, by Daniel Gruss et al., June 2017.
- Flipping Bits in Memory Without Accessing Them : An Experimental Study of DRAM Disturbance Errors, by Yoongu Kim et al., June 2014.
- https://meltdownattack.com
