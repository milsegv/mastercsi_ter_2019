#include <unistd.h>
#include <stdio.h>
#include <inttypes.h>
#include <x86intrin.h>

#include "libmy_shared.h"

int main() {
    printf("Victim process started\n");
    sleep(1);
    while (1) {
    	printf ("Accessing variable..\n");
	    execute_stuff_on_variable();
        for (int i = 0; i < 10000000; i++); // idle
        //sleep(1);
    }
}
