## Flush+Reload PoC
### Usage
First, set `LD_LIBRARY_PATH` correctly:

`export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:.`

then, to launch the PoC: `bash poc.sh <CACHE_HIT_THRESOLD>`

`CACHE_HIT_THRESHOLD`: number of clock cycles telling a variable is located in the L3 Cache.
As this value is difficult to tell, we advise the user to set it empirically (that's what we did),
or use a software to guess this threshold. On a i3-2350M laptop, we set it to 185 beacause
the results were meaningful.

Once one is done with the PoC, one must not forget to kill the process running on background: `pkill -9 victim` before
launching the PoC again.