#!/bin/bash

if [ $# -eq 0 ]
then
	>&2 echo "Usage: bash poc.sh <CACHE_HIT_THRESHOLD>"
	exit 1
fi

make
if [ $? -ne 0 ]
then
	>&2 echo "Error building program, exiting"
	exit 1
fi

CACHE_HIT_THRESHOLD=$1

for i in $(seq 1 1)
do
	taskset 0x1 ./victim &
done
taskset 0x1 ./flush_reload $CACHE_HIT_THRESHOLD
