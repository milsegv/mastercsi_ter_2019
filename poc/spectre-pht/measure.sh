#!/bin/sh

OUT=data.dat
NB_MEASURE=1000
EXE=./spectre-64.out
SECRET=secret

mv $SECRET $SECRET.bak

list="32 64 128 256 512 1024"
list=$(seq 32 32 512)

# For loop, 32, 64, 96,...640
for l in $(seq 1 20)
do
	# Set up a string of the desired length
	echo -n "char* secret = " > $SECRET
	python2 -c "print '\"The Magic Words are Squeamish Ossifrage.\"'*$l" >> $SECRET
	echo -n ";"	>> $SECRET

	make clean &> /dev/null
	rm -f $OUT &> /dev/null
	make &> /dev/null

	for i in $(seq 1 $NB_MEASURE)
	do 
		$EXE | grep "ms" | tr ": " "\n" | tail -n 1 >> $OUT # Computes values
	done

	mean_time=$(awk '{S+=$1}END{print S/NR}' $OUT)
	length=$(bc <<< "$l*32")
    byte_rate=$(bc <<< "$length/$mean_time")
	echo -e "Length: $length\tMoyenne des temps: " $mean_time "ms" " \tByte rate: "$byte_rate " bytes/s"
	
	rm -f $SECRET &> /dev/null
done

mv $SECRET.bak $SECRET
